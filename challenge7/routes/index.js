var express = require('express');
var router = express.Router();

const pageRouter = require('./page')
const authRouter = require('./auth')
const gamesRouter = require('./games')
const roomsRouter = require('./rooms')


router.use("/page",pageRouter);
router.use("/auth",authRouter);
router.use("/games",gamesRouter);
router.use("/rooms",roomsRouter);



module.exports = router;
