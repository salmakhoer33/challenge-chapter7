const router = require('express').Router();
const passport = require ("passport");
const games = require("../controllers/gameController");


router.post("/multiplayer",games.multiplayer);
router.post("/roomCreate",games.roomCreate);
router.get("/roomCreate",games.roomCreate);
router.get("/getAll",games.getAll);

module.exports = router ;