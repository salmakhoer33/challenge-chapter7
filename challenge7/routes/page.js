const router = require('express').Router();
const page = require("../controllers/pageController");

const restrict = (req, res, next) => {
    if (req.isAuthenticated()) return next();

    res.redirect("/page/login");
};

router.get("/",page.home);
router.get("/login",page.login);
router.get("/rooms",page.histories);
router.get("/register",page.register);

module.exports = router;