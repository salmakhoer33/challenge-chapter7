const router = require('express').Router();
const passport = require ("passport");
const auth = require("../controllers/authController");


router.post("/login",auth.login);
router.post("/register",auth.register);
router.get("/registerAll",auth.registerAll);

//  router.post(
//      "/login",
//      passport.authenticate("local", {
//          successRedirect: "/login",
//          failureRedirect: "/page/login",
//      })
//  )


module.exports = router ;