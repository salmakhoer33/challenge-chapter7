'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
    queryInterface.changeColumn("Rooms", "player1",{
      type:'INTEGER USING CAST("player1" as INTEGER)',
      allowNull: true,
    }),
  ])
  },


  async down (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.changeColumn("Rooms", "player1",{
        type: Sequelize.STRING,
        allowNull: true,
      }),
    ])
    },
    };
