'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
  },
  

  async down (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.removeColumn("Games", "UserId",{
        type: Sequelize.STRING,
        allowNull: true,
      }),
    queryInterface.removeColumn("Games", "RoomId",{
      type: Sequelize.STRING,
      allowNull: true,
    }),
    queryInterface.removeColumn("Rooms", "player1",{
      type: Sequelize.STRING,
      allowNull: true,
    }),
  ])
  },
  };

