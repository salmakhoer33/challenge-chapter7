const {User} = require("../models");


const format = (user) => {
    const {id, username} = user;

    return {
        id, 
        username,
        token: user.generateToken(),
    }
}

const checkCredential = ({username,password }) => {
    const data = User.find(
        (row)=> row.username === username);
    return new Promise((resolve,reject) => {
        if (!data) return reject ("users not found");
       if(data.password === password) return resolve(data);

       reject('Wrong Credential!');
    });
 };

 exports.login = (req, res) => {
    User.authentication(req.body)
    .then((data)=> {
        res.json({status: 'Login Success',data: format(data) });
    })
    .catch((err)=> {
        res.status(500).json ({status: 'Login Failed', msg : err});
    });
};

exports.register = (req, res) => {

    User.register(req.body)
    .then((data)=> {
        res.json({status: 'Register Success',data });
    })
    .catch((err)=> {
        res.status(500).json ({status: 'Register Failed', msg : err});
    });
};

exports.registerAll = (req,res) => {

    User.findAll().then((users) => {
       res.json({ status: "Fetch Success", data: users});
    }).catch(err=>{
       res.status(500).json({ status: "Register Failed", msg: err });
    });
 };

