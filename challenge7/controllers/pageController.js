const {Rooms} = require ("../models")

exports.home = (req,res) => {
const title = "Hello World"
const subtitle = "Welcome to the world"

res.render("index",{title,subtitle});
};

exports.login = (req,res) => {
    const title = "Login Page";

    res.render("login",{title});
};
exports.register= (req,res) => {
    const title = "Register Page";

    res.render("register",{title});
};

exports.histories = (req,res) => {
    const title = "History Page";
    Rooms.findAll({include:["roomGame"]}).then((hasil) => {
        res.render("rooms",{title,hasil})
    })
};

