const {Rooms} = require('../models');


exports.ruang = (req, res) => {

    Rooms.ruang(req.body)
    .then((result)=> {
        res.json({status: 'Create Room Success',result });
    })
    .catch((err)=> {
        res.status(500).json ({status: 'Create Room Failed!', msg : err});
    });
};

exports.ruangAll= (req,res) => {

    Rooms.findAll().then((data) => {
       res.json({ status: "Fetch Success", data: data});
    }).catch(err=>{
       res.status(500).json({ status: "Register Failed", msg: err });
    });
 };

