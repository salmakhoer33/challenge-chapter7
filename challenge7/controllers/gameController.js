const {Games,Rooms} = require("../models");


const findWinner = (first, second) => {
    if (first === "rock") {
        if (second === "rock") {
            return "draw";
        }
        if (second === "scissor") {
            return "win";
        }
        if (second === "paper") {
            return "lose";
        }
    }

    if (first === "paper") {
        if (second === "rock") {
            return "win";
        }
        if (second === "scissor") {
            return "lose";
        }
        if (second === "paper") {
            return "draw";
        }
    }

    if (first === "scissor") {
        if (second === "rock") {
            return "lose";
        }
        if (second === "scissor") {
            return "draw";
        }
        if (second === "paper") {
            return "win";
        }
    }
};


exports.multiplayer = async (req, res) => {
    const {UserId, RoomId,result} = req.body;

    const isNewGame = await Games.findAll({ where: { RoomId:RoomId}});
    const isUserExist = await Games.findOne({
        where: { RoomId : RoomId, UserId: UserId },
    });

    if (isNewGame.length === 0) {
        Games.create( req.body )
        .then ((respon) => {
            res.status(201).json({ status : "Create Game Success", respon });
        })
        .catch((err)=> {
            res.status(400).json({ status: "Create Game Failed", message: err });
        });
    } else if (isNewGame.length === 1) {
        if (isNewGame.length === 1) {
            if (!isUserExist) {
                const gameResult = findWinner(isNewGame[0].result, result);

                Games.create( req.body)
                .then(() => {
                    Rooms.update(
                        { result : gameResult, player1: isNewGame[0].UserId},
                        { where: { id: RoomId}}
                    )
                    .then(() => {
                        res.json({ result: `Player 1 ${gameResult}`});
                    })
                    .catch((err) => {
                        res
                        .status(400)
                        .json({ status: "Create Game Failed", message: err });
                    });
                })
                .catch((err) => {
                    res
                    .status(400)
                    .json({ status: "Create Game Failed", message: err });
                });
            } else {
                res.status(400).json({
                    status: "Create Game Failed",
                    message: "This player is inserted!",
                });
            }
        }
    } else {
        res.status(400).json({
            status: "Create Game Failed",
            message: "Changing the game room",
        });
}
};

exports.roomCreate = (req,res ) =>{
    Rooms.create (req.body)
    .then((data) => {
        res.json({message: "Create Success", data})
    }).catch ((err) => {
        res.json({message: "Create Was Failed", data})
    })
};

exports.getAll = (req,res) => {

    Rooms.findAll({include:["roomGame"]}).then((hasil) => {
       res.json({ status: "Fetch Success", data: hasil});
    }).catch(err=>{
       res.status(500).json({ status: "Register Failed", msg: err });
    });
 };


